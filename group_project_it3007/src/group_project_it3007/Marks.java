package group_project_it3007;

import javax.swing.*;
import java.awt.*;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.*;

public class Marks {
	
	   
	
    public void MK() {
    	
        JFrame frame = new JFrame("My First GUI");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(850,650);
        frame.getContentPane().setBackground(new Color(5, 65, 90));


        JTextField mark1 = new JTextField();
        mark1.setBounds(32, 144, 100, 30);
        JTextField name1 = new JTextField();
        name1.setBounds(142, 144, 200, 30);

        JTextField mark2 = new JTextField();
        mark2.setBounds(32, 184, 100, 30);
        JTextField name2 = new JTextField();
        name2.setBounds(142, 184, 200, 30);

        JTextField mark3 = new JTextField();
        mark3.setBounds(32, 224, 100, 30);
        JTextField name3 = new JTextField();
        name3.setBounds(142, 224, 200, 30);

        JTextField mark4 = new JTextField();
        mark4.setBounds(32, 264, 100, 30);
        JTextField name4 = new JTextField();
        name4.setBounds(142, 264, 200, 30);

        JTextField mark5 = new JTextField();
        mark5.setBounds(32, 304, 100, 30);
        JTextField name5 = new JTextField();
        name5.setBounds(142, 304, 200, 30);

        JTextField mark6 = new JTextField();
        mark6.setBounds(32, 344, 100, 30);
        JTextField name6 = new JTextField();
        name6.setBounds(142, 344, 200, 30);

        JTextField mark7 = new JTextField();
        mark7.setBounds(32, 384, 100, 30);
        JTextField name7 = new JTextField();
        name7.setBounds(142, 384, 200, 30);



        JTextField sort1 = new JTextField();
        sort1.setBounds(481, 144, 100, 30);
        JTextField sortname1 = new JTextField();
        sortname1.setBounds(601, 144, 200, 30);

        JTextField sort2 = new JTextField();
        sort2.setBounds(481, 184, 100, 30);
        JTextField sortname2 = new JTextField();
        sortname2.setBounds(601, 184, 200, 30);

        JTextField sort3 = new JTextField();
        sort3.setBounds(481, 224, 100, 30);
        JTextField sortname3 = new JTextField();
        sortname3.setBounds(601, 224, 200, 30);

        JTextField sort4 = new JTextField();
        sort4.setBounds(481, 264, 100, 30);
        JTextField sortname4 = new JTextField();
        sortname4.setBounds(601, 264, 200, 30);

        JTextField sort5 = new JTextField();
        sort5.setBounds(481, 304, 100, 30);
        JTextField sortname5 = new JTextField();
        sortname5.setBounds(601, 304, 200, 30);

        JTextField sort6 = new JTextField();
        sort6.setBounds(481, 344, 100, 30);
        JTextField sortname6 = new JTextField();
        sortname6.setBounds(601, 344, 200, 30);

        JTextField sort7 = new JTextField();
        sort7.setBounds(481, 384, 100, 30);
        JTextField sortname7 = new JTextField();
        sortname7.setBounds(601, 384, 200, 30);
        
     




        JButton done = new JButton();
        done.setFont(new Font("Tahoma", Font.BOLD, 18));
        done.setBounds(473,489, 100,80);
        done.setText("Sort");
        done.addActionListener(new ActionListener() {
               public void actionPerformed(ActionEvent e) {
                   String s1= mark1.getText();
                   String s2= mark2.getText();
                   String s3= mark3.getText();
                   String s4= mark4.getText();
                   String s5= mark5.getText();
                   String s6= mark6.getText();
                   String s7= mark7.getText();
                   
                   
                   String nameA= name1.getText();
                   String nameB= name2.getText();
                   String nameC= name3.getText();
                   String nameD= name4.getText();
                   String nameE= name5.getText();
                   String nameF= name6.getText();
                   String nameG= name7.getText();
                   
                   HashMap<Integer, String> dict
                   = new HashMap<Integer, String>();
                   
                  
               // Inserting the values into dictionary
               dict.put(Integer.parseInt(s1), nameA);
               dict.put(Integer.parseInt(s2), nameB);
               dict.put(Integer.parseInt(s3), nameC);
               dict.put(Integer.parseInt(s4), nameD);
               dict.put(Integer.parseInt(s5), nameE);
               dict.put(Integer.parseInt(s6), nameF);
               dict.put(Integer.parseInt(s7), nameG);
               
               
               ////////////get key value for sorting///////////////////
               
               ArrayList<Integer> list=new ArrayList<Integer>(); 
            
	             for (Map.Entry<Integer, String> entry :dict.entrySet()){
	
	            	   list.add(entry.getKey());
	              	    	
	             	}
             
	             Object[] objArray = list.toArray();
             
            
	             int length = objArray.length;
	             int intArray[] = new int[length];
	             for(int i=0; i<length; i++){
	                intArray[i] = (int) objArray[i];
	             }
           

               	Marks ob = new Marks();                         
               	ob.sort(intArray);
               	
               	System.out.println("int  array: "+Arrays.toString(intArray));
               	System.out.println("string  array: "+intArray[0]);
		
               	//////////////get string value of the array///////////////////
               	
                ArrayList<String> list2=new ArrayList<String>(); 
               	
               	for(int i=0;i<intArray.length;i++) {
               		
            		String key=dict.get(intArray[i]);
            		
            		list2.add(key);
            			
            	}
               	
               	Object[] objArray2 = list2.toArray();
               	
   
               	int length2 = objArray2.length;
                String strArray[] = new String[length];
                for(int i=0; i<length; i++){
                   strArray[i] = (String) objArray2[i];
                }
                
                System.out.println("string  array: "+Arrays.toString(strArray));
                
                System.out.println("string  array: "+strArray[0]);
                
                
                sort1.setText(String.valueOf(intArray[0]));
                sort2.setText(String.valueOf(intArray[1]));
                sort3.setText(String.valueOf(intArray[2]));
                sort4.setText(String.valueOf(intArray[3]));
                sort5.setText(String.valueOf(intArray[4]));
                sort6.setText(String.valueOf(intArray[5]));
                sort7.setText(String.valueOf(intArray[6]));
                
                sortname1.setText(strArray[0]);
                sortname2.setText(strArray[1]);
                sortname3.setText(strArray[2]);
                sortname4.setText(strArray[3]);
                sortname5.setText(strArray[4]);
                sortname6.setText(strArray[5]);
                sortname7.setText(strArray[6]);
                
                
                
                
               	
		           
		           
		       }
		   });
        
        
        ///////////////////////////////////////////////////////////////////////////////////////



        frame.getContentPane().add(mark1);
        frame.getContentPane().add(mark3);
        frame.getContentPane().add(mark5);
        frame.getContentPane().add(mark7);
        frame.getContentPane().add(mark2);
        frame.getContentPane().add(mark4);
        frame.getContentPane().add(mark6);

        frame.getContentPane().add(name1);
        frame.getContentPane().add(name3);
        frame.getContentPane().add(name5);
        frame.getContentPane().add(name7);
        frame.getContentPane().add(name2);
        frame.getContentPane().add(name4);
        frame.getContentPane().add(name6);

        frame.getContentPane().add(sort1);
        frame.getContentPane().add(sort3);
        frame.getContentPane().add(sort5);
        frame.getContentPane().add(sort7);
        frame.getContentPane().add(sort2);
        frame.getContentPane().add(sort4);
        frame.getContentPane().add(sort6);

        frame.getContentPane().add(sortname1);
        frame.getContentPane().add(sortname3);
        frame.getContentPane().add(sortname5);
        frame.getContentPane().add(sortname7);
        frame.getContentPane().add(sortname2);
        frame.getContentPane().add(sortname4);
        frame.getContentPane().add(sortname6);


        frame.getContentPane().add(done);
        frame.getContentPane().setLayout(null);
        
        JLabel lblNewLabel = new JLabel("Marks");
        lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 18));
        lblNewLabel.setForeground(Color.WHITE);
        lblNewLabel.setBounds(52, 119, 73, 14);
        frame.getContentPane().add(lblNewLabel);
        
        JLabel lblStudentName = new JLabel("Student Name");
        lblStudentName.setForeground(Color.WHITE);
        lblStudentName.setFont(new Font("Tahoma", Font.BOLD, 18));
        lblStudentName.setBounds(174, 119, 150, 14);
        frame.getContentPane().add(lblStudentName);
        
        JLabel lblNewLabel_1 = new JLabel("Marks");
        lblNewLabel_1.setForeground(Color.WHITE);
        lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 18));
        lblNewLabel_1.setBounds(52, 119, 73, 14);
        frame.getContentPane().add(lblNewLabel_1);
        
        JLabel lblStudentName_1 = new JLabel("Student Name");
        lblStudentName_1.setForeground(Color.WHITE);
        lblStudentName_1.setFont(new Font("Tahoma", Font.BOLD, 18));
        lblStudentName_1.setBounds(174, 119, 150, 14);
        frame.getContentPane().add(lblStudentName_1);
        
        JLabel lblStudentName_2 = new JLabel("Student Name");
        lblStudentName_2.setForeground(Color.WHITE);
        lblStudentName_2.setFont(new Font("Tahoma", Font.BOLD, 18));
        lblStudentName_2.setBounds(628, 119, 150, 14);
        frame.getContentPane().add(lblStudentName_2);
        
        JLabel lblNewLabel_2 = new JLabel("Marks");
        lblNewLabel_2.setForeground(Color.WHITE);
        lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 18));
        lblNewLabel_2.setBounds(500, 119, 73, 14);
        frame.getContentPane().add(lblNewLabel_2);
        
        JLabel lblNewLabel_3 = new JLabel("Marks Sort Panel");
        lblNewLabel_3.setForeground(Color.YELLOW);
        lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 30));
        lblNewLabel_3.setBounds(287, 54, 311, 30);
        frame.getContentPane().add(lblNewLabel_3);
        
        JButton btnNewButton = new JButton("Back");
        btnNewButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		frame.dispose();
        		new Main();
        	}
        });
        btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 18));
        btnNewButton.setBounds(253, 489, 100, 80);
        frame.getContentPane().add(btnNewButton);
        frame.setVisible(true);



    }
    
  
    void sort(int arr[]) {
        int n = arr.length;
        for (int i = 1; i < n; ++i) {
            int key = arr[i];
            int j = i - 1;
            
            while (j >= 0 && arr[j] >=key) {
                arr[j + 1] = arr[j];
                j = j - 1;
            }
            arr[j + 1] = key;
        }
         
    }
}
