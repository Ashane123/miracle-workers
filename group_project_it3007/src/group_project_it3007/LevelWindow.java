package group_project_it3007;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class LevelWindow extends JPanel implements ActionListener,KeyListener {

	JFrame window = new JFrame();
	String[] s = {"Beginners-Level", "Middle-Level", "Highest-Level"};
	JComboBox nameOfLevels = new JComboBox(s);
	JButton enterButton = new JButton();
	JButton backButton = new JButton();
	JButton instructionButton = new JButton();
	JLabel lblNewLabel = new JLabel();
	

	Font customFont = new Font("Bold",Font.BOLD,30);

	Font comboFont = new Font("Italic",Font.ITALIC,25);
	
	LevelWindow(){
		this.setBackground(Color.BLACK);
		window.add(this);
		
		nameOfLevels.setFont(comboFont);
	    nameOfLevels.setBackground(Color.black);
		nameOfLevels.setForeground(Color.red);
		window.add(nameOfLevels,BorderLayout.PAGE_START);
		
		backButton.setFont(customFont);
		backButton.setText("Back");
		backButton.setBackground(new Color(139, 0, 0));
	    backButton.setForeground(Color.white);
	    backButton.addActionListener(this);
	    backButton.addKeyListener(this);
		window.add(backButton,BorderLayout.WEST);
		
		enterButton.setFont(customFont);
		enterButton.setText("ENTER");
		enterButton.setBackground(new Color(139, 0, 0));
	    enterButton.setForeground(Color.white);
	    enterButton.addActionListener(this);
	    enterButton.addKeyListener(this);
		window.add(enterButton,BorderLayout.EAST);
		
		instructionButton.setFont(customFont);
		instructionButton.setText("INSTRUCTIONS");
		instructionButton.setBackground(new Color(139, 0, 0));
		instructionButton.setForeground(Color.white);
		instructionButton.addActionListener(this);
		instructionButton.addKeyListener(this);
		window.add(instructionButton,BorderLayout.PAGE_END);
		
		
		lblNewLabel.setIcon(new ImageIcon("names3.png"));
		//lblNewLabel.setBounds(182, 22, 401, 422);
		window.add(lblNewLabel,BorderLayout.CENTER);
	//	window.getContentPane().add(lblNewLabel);
		
		
		window.setUndecorated(true);
		window.setSize(773,580);
		window.setLocation(280,50);
		window.setVisible(true);
		
	}
	@Override
	public void keyTyped(KeyEvent g) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent kp) {
		if(kp.getKeyCode()==KeyEvent.VK_ESCAPE) {
			window.dispose();
			//MainFrame m = new MainFrame();
		} else if(kp.getKeyCode()==KeyEvent.VK_ENTER&&nameOfLevels.getSelectedIndex()==0){
			JOptionPane.showMessageDialog(null,"Beginners level is not ready");
		}else if(kp.getKeyCode()==KeyEvent.VK_ENTER&&nameOfLevels.getSelectedIndex()==1){
			JOptionPane.showMessageDialog(null,"Middle level is not ready");
		}else if(kp.getKeyCode()==KeyEvent.VK_ENTER&&nameOfLevels.getSelectedIndex()==2){
			JOptionPane.showMessageDialog(null,"Highest level is not ready");
		}
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		if (ae.getSource()==backButton) {
			window.dispose();
			Main m = new Main();
		}else if (ae.getSource()== instructionButton) {
			window.dispose();
			InstructionWindow window = new InstructionWindow();
			window.ins();
		}
		
		else if (ae.getSource()==enterButton&&nameOfLevels.getSelectedIndex()==0) {
			window.dispose();
			GamePlay1 g1 = new GamePlay1();
			g1.gp1();
		}else if (ae.getSource()==enterButton&&nameOfLevels.getSelectedIndex()==1) {
			window.dispose();
			GamePlay2 g2 = new GamePlay2();
			g2.gp2();
		}else if (ae.getSource()==enterButton&&nameOfLevels.getSelectedIndex()==2) {
			JOptionPane.showMessageDialog(null,"Highest level is still in maintainance mode");
		}
	}

}

