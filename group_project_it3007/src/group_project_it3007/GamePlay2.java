package group_project_it3007;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.ImageIcon;

public class GamePlay2 {

	private JFrame frame;
	private JTextField H1;
	private JTextField D1;
	private JTextField H4;
	private JTextField D4;
	private JTextField H2;
	private JTextField D2;
	private JTextField H5;
	private JTextField D5;
	private JTextField H3;
	private JTextField D3;
	private JTextField H6;
	private JTextField D6;
	int abcd;

	/**
	 * Launch the application.
	 */
	public void gp2() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GamePlay2 window = new GamePlay2();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GamePlay2() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Middle Level");
		frame.getContentPane().setBackground(new Color(245, 222, 179));
		frame.setBounds(100, 100, 773, 610);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel tree1 = new JLabel("New label");
		tree1.setIcon(new ImageIcon("M1C.png"));
		tree1.setBounds(43, 22, 145, 118);
		frame.getContentPane().add(tree1);
		
		JLabel lblNewLabel = new JLabel("Height :");
		lblNewLabel.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel.setBounds(25, 152, 63, 19);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Depth (E):");
		lblNewLabel_1.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_1.setBounds(25, 182, 63, 17);
		frame.getContentPane().add(lblNewLabel_1);
		
		String[] s = {"Yes", "No"};
		JComboBox C1 = new JComboBox(s);
		C1.setBounds(166, 208, 63, 24);
		Font comboFont = new Font("Italic",Font.PLAIN,15);
		frame.getContentPane().add(C1);
		
		JLabel lblNewLabel_2 = new JLabel("Balanced Binary Tree?");
		lblNewLabel_2.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_2.setBounds(25, 210, 145, 19);
		frame.getContentPane().add(lblNewLabel_2);
		
		H1 = new JTextField();
		H1.setBounds(102, 150, 86, 20);
		frame.getContentPane().add(H1);
		H1.setColumns(10);
		
		D1 = new JTextField();
		D1.setBounds(102, 179, 86, 20);
		frame.getContentPane().add(D1);
		D1.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("Height :");
		lblNewLabel_3.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_3.setBounds(25, 422, 63, 19);
		frame.getContentPane().add(lblNewLabel_3);
		
		JLabel lblNewLabel_2_1 = new JLabel("Balanced Binary Tree?");
		lblNewLabel_2_1.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_2_1.setBounds(25, 483, 145, 19);
		frame.getContentPane().add(lblNewLabel_2_1);
		
		JLabel lblNewLabel_1_1 = new JLabel("Depth (I):");
		lblNewLabel_1_1.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_1_1.setBounds(25, 455, 63, 17);
		frame.getContentPane().add(lblNewLabel_1_1);
		
		H4 = new JTextField();
		H4.setColumns(10);
		H4.setBounds(102, 422, 86, 20);
		frame.getContentPane().add(H4);
		
		D4 = new JTextField();
		D4.setColumns(10);
		D4.setBounds(102, 454, 86, 20);
		frame.getContentPane().add(D4);
		
		JComboBox C4 = new JComboBox(s);
		C4.setBounds(166, 481, 63, 24);
		frame.getContentPane().add(C4);
		
		JLabel tree1_1 = new JLabel("New label");
		tree1_1.setIcon(new ImageIcon("M4C.png"));
		tree1_1.setBounds(43, 268, 145, 118);
		frame.getContentPane().add(tree1_1);
		
		JLabel tree1_2 = new JLabel("New label");
		tree1_2.setIcon(new ImageIcon("M2C.png"));
		tree1_2.setBounds(294, 23, 145, 116);
		frame.getContentPane().add(tree1_2);
		
		JLabel tree1_3 = new JLabel("New label");
		tree1_3.setIcon(new ImageIcon("M3C.png"));
		tree1_3.setBounds(552, 22, 145, 118);
		frame.getContentPane().add(tree1_3);
		
		JLabel tree1_4 = new JLabel("New label");
		tree1_4.setIcon(new ImageIcon("M5C.png"));
		tree1_4.setBounds(295, 268, 144, 118);
		frame.getContentPane().add(tree1_4);
		
		JLabel tree1_5 = new JLabel("New label");
		tree1_5.setIcon(new ImageIcon("M6C.png"));
		tree1_5.setBounds(549, 267, 138, 120);
		frame.getContentPane().add(tree1_5);
		
		JLabel lblNewLabel_4 = new JLabel("Height :");
		lblNewLabel_4.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_4.setBounds(280, 152, 63, 19);
		frame.getContentPane().add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Height :");
		lblNewLabel_5.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_5.setBounds(523, 155, 63, 19);
		frame.getContentPane().add(lblNewLabel_5);
		
		JLabel lblNewLabel_4_1 = new JLabel("Height :");
		lblNewLabel_4_1.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_4_1.setBounds(280, 422, 63, 19);
		frame.getContentPane().add(lblNewLabel_4_1);
		
		JLabel lblNewLabel_4_2 = new JLabel("Height :");
		lblNewLabel_4_2.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_4_2.setBounds(523, 425, 63, 19);
		frame.getContentPane().add(lblNewLabel_4_2);
		
		JLabel lblNewLabel_1_2 = new JLabel("Depth (H):");
		lblNewLabel_1_2.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_1_2.setBounds(523, 455, 69, 17);
		frame.getContentPane().add(lblNewLabel_1_2);
		
		JLabel lblNewLabel_1_3 = new JLabel("Depth (F):");
		lblNewLabel_1_3.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_1_3.setBounds(280, 454, 63, 17);
		frame.getContentPane().add(lblNewLabel_1_3);
		
		JLabel lblNewLabel_1_4 = new JLabel("Depth (G):");
		lblNewLabel_1_4.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_1_4.setBounds(280, 182, 69, 17);
		frame.getContentPane().add(lblNewLabel_1_4);
		
		JLabel lblNewLabel_1_5 = new JLabel("Depth (H):");
		lblNewLabel_1_5.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_1_5.setBounds(523, 182, 69, 17);
		frame.getContentPane().add(lblNewLabel_1_5);
		
		JLabel lblNewLabel_2_2 = new JLabel("Balanced Binary Tree?");
		lblNewLabel_2_2.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_2_2.setBounds(280, 213, 145, 19);
		frame.getContentPane().add(lblNewLabel_2_2);
		
		JLabel lblNewLabel_2_3 = new JLabel("Balanced Binary Tree?");
		lblNewLabel_2_3.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_2_3.setBounds(280, 483, 145, 19);
		frame.getContentPane().add(lblNewLabel_2_3);
		
		JLabel lblNewLabel_2_4 = new JLabel("Balanced Binary Tree?");
		lblNewLabel_2_4.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_2_4.setBounds(523, 483, 145, 19);
		frame.getContentPane().add(lblNewLabel_2_4);
		
		JLabel lblNewLabel_2_5 = new JLabel("Balanced Binary Tree?");
		lblNewLabel_2_5.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_2_5.setBounds(523, 210, 145, 19);
		frame.getContentPane().add(lblNewLabel_2_5);
		
		JComboBox C2 = new JComboBox(s);
		C2.setBounds(423, 208, 63, 24);
		frame.getContentPane().add(C2);
		
		JComboBox C3 = new JComboBox(s);
		C3.setBounds(666, 208, 63, 24);
		frame.getContentPane().add(C3);
		
		JComboBox C6 = new JComboBox(s);
		C6.setBounds(666, 481, 63, 24);
		frame.getContentPane().add(C6);
		
		JComboBox C5 = new JComboBox(s);
		C5.setBounds(423, 481, 63, 24);
		frame.getContentPane().add(C5);
		
		H2 = new JTextField();
		H2.setColumns(10);
		H2.setBounds(359, 152, 86, 20);
		frame.getContentPane().add(H2);
		
		D2 = new JTextField();
		D2.setColumns(10);
		D2.setBounds(359, 181, 86, 20);
		frame.getContentPane().add(D2);
		
		H5 = new JTextField();
		H5.setColumns(10);
		H5.setBounds(359, 422, 86, 20);
		frame.getContentPane().add(H5);
		
		D5 = new JTextField();
		D5.setColumns(10);
		D5.setBounds(359, 452, 86, 20);
		frame.getContentPane().add(D5);
		
		H3 = new JTextField();
		H3.setColumns(10);
		H3.setBounds(601, 152, 86, 20);
		frame.getContentPane().add(H3);
		
		D3 = new JTextField();
		D3.setColumns(10);
		D3.setBounds(601, 181, 86, 20);
		frame.getContentPane().add(D3);
		
		H6 = new JTextField();
		H6.setColumns(10);
		H6.setBounds(601, 422, 86, 20);
		frame.getContentPane().add(H6);
		
		D6 = new JTextField();
		D6.setColumns(10);
		D6.setBounds(601, 454, 86, 20);
		frame.getContentPane().add(D6);
		
		JButton btnNewButton = new JButton("OK");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				resultMiddle m = new resultMiddle();
				m.resultM();
				
				
				String x1 = C1.getSelectedItem().toString();
				int c1;
				if (x1 == "No")
				{ c1=1;}else {c1 = 0;}
				
				String x2 = C2.getSelectedItem().toString();
				int c2;
				if (x2 == "Yes"){	c2=1;}else {c2 = 0;}
				
				String x3 = C3.getSelectedItem().toString();
				int c3;
				if (x3 == "No")
				{ c3=1;}else {c3 = 0;}
				
				String x4 = C4.getSelectedItem().toString();
				int c4;
				if (x4 == "No")
				{ c4=1;}else {c4 = 0;}
				
				String x5 = C5.getSelectedItem().toString();
				int c5;
				if (x5 == "No")
				{ c5=1;}else {c5 = 0;}
				
				String x6 = C6.getSelectedItem().toString();
				int c6;
				if (x6 == "No")
				{ c6=1;}else {c6 = 0;}
				
				int total=0;
				//height checking
				//1st answer
				String h1 = H1.getText();
				if(h1.equals("2")) {
					total = total+1;
				}else{
					total = total;
				}
				//4th answer
				String h2 = H2.getText();
				if(h2.equals("3")) {
					total = total+1;
				}else{
					total = total;
				}
				
				//7th answer
				
				String h3 = H3.getText();
				if(h3.equals("3")) {
					total = total+1;
				}else{
					total = total;
				}
				//10th answer
				
				String h4 = H4.getText();
				if(h4.equals("3")) {
					total = total+1;
				}else{
					total = total;
				}
				//13th answer
				
				String h5 = H5.getText();
				if(h5.equals("4")) {
					total = total+1;
				}else{
					total = total;
				}
				//16th answer
				
				String h6 = H6.getText();
				if(h6.equals("3")) {
					total = total+1;
				}else{
					total = total;
				}
				//depth checking
				//2nd answer
				
				String d1 = D1.getText();
				if(d1.equals("2")) {
					total = total+1;
				}else{
					total = total;
				}
				//5th answer
				
				String d2 = D2.getText();
				if(d2.equals("3")) {
					total = total+1;
				}else{
					total = total;
				}
				//8th answer
				
				String d3 = D3.getText();
				if(d3.equals("3")) {
					total = total+1;
				}else{
					total = total;
				}
				//11th answer
				
				String d4 = D4.getText();
				if(d4.equals("3")) {
					total = total+1;
				}else{
					total = total;
				}
				//14th answer
				
				String d5 = D5.getText();
				if(d5.equals("2")) {
					total = total+1;
				}else{
					total = total;
				}
				//17th answer
				
				String d6 = D6.getText();
				if(d6.equals("3")) {
					total = total+1;
				}else{
					total = total;
				}
				
				abcd = c1+c2+c3+c4+c5+c6;
				
				JOptionPane.showMessageDialog(null,"Your Score is "+(total+abcd));

				
			}
		});
		btnNewButton.setBounds(391, 523, 96, 29);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnBack = new JButton("BACK");
		btnBack.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae) {
				frame.dispose();
				LevelWindow lw = new LevelWindow();
			}
		});
		btnBack.setBounds(267, 523, 96, 29);
		frame.getContentPane().add(btnBack);
	}
	
	

}
