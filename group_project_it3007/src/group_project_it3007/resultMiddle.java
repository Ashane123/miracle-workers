package group_project_it3007;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;

public class resultMiddle {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void resultM() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					resultMiddle window = new resultMiddle();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public resultMiddle() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Biginners Level");
		frame.getContentPane().setBackground(new Color(173, 216, 230));
		frame.setBounds(100, 100, 773, 610);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setLocation(280,50);
		JLabel tree1 = new JLabel("");
		tree1.setIcon(new ImageIcon("M1C.png"));
		tree1.setBounds(53, 21, 145, 120);
		frame.getContentPane().add(tree1);
		
		JLabel lblNewLabel = new JLabel("Height :");
		lblNewLabel.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel.setBounds(25, 152, 63, 19);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Depth (B):");
		lblNewLabel_1.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_1.setBounds(25, 182, 64, 17);
		frame.getContentPane().add(lblNewLabel_1);
		
		String[] s = {"Yes", "No"};
		Font comboFont = new Font("Italic",Font.PLAIN,15);
		
		JLabel lblNewLabel_2 = new JLabel("Not a Balanced Binary Tree");
		lblNewLabel_2.setForeground(Color.RED);
		lblNewLabel_2.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_2.setBounds(25, 210, 173, 19);
		frame.getContentPane().add(lblNewLabel_2);
		
		
	
		JLabel lblNewLabel_3 = new JLabel("Height :");
		lblNewLabel_3.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_3.setBounds(25, 422, 63, 19);
		frame.getContentPane().add(lblNewLabel_3);
		
		JLabel lblNewLabel_2_1 = new JLabel("Not a Balanced Binary Tree");
		lblNewLabel_2_1.setForeground(Color.RED);
		lblNewLabel_2_1.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_2_1.setBounds(25, 483, 145, 19);
		frame.getContentPane().add(lblNewLabel_2_1);
		
		JLabel lblNewLabel_1_1 = new JLabel("Depth (E):");
		lblNewLabel_1_1.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_1_1.setBounds(25, 455, 69, 17);
		frame.getContentPane().add(lblNewLabel_1_1);
		
		JLabel tree1_1 = new JLabel("");
		tree1_1.setIcon(new ImageIcon("M4C.png"));
		tree1_1.setBounds(53, 278, 145, 120);
		frame.getContentPane().add(tree1_1);
		
		JLabel tree1_2 = new JLabel("");
		tree1_2.setIcon(new ImageIcon("M2C.png"));
		tree1_2.setBounds(322, 21, 135, 120);
		frame.getContentPane().add(tree1_2);
		
		JLabel tree1_3 = new JLabel("");
		tree1_3.setIcon(new ImageIcon("M3C.PNG"));
		tree1_3.setBounds(555, 21, 135, 114);
		frame.getContentPane().add(tree1_3);
		
		JLabel tree1_4 = new JLabel("New label");
		tree1_4.setIcon(new ImageIcon("M5C.png"));
		tree1_4.setBounds(301, 278, 144, 120);
		frame.getContentPane().add(tree1_4);
		
		JLabel tree1_5 = new JLabel("");
		tree1_5.setIcon(new ImageIcon("M6C.png"));
		tree1_5.setBounds(555, 278, 145, 120);
		frame.getContentPane().add(tree1_5);
		
		JLabel lblNewLabel_4 = new JLabel("Height :");
		lblNewLabel_4.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_4.setBounds(280, 152, 63, 19);
		frame.getContentPane().add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Height :");
		lblNewLabel_5.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_5.setBounds(523, 155, 63, 19);
		frame.getContentPane().add(lblNewLabel_5);
		
		JLabel lblNewLabel_4_1 = new JLabel("Height :");
		lblNewLabel_4_1.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_4_1.setBounds(280, 422, 63, 19);
		frame.getContentPane().add(lblNewLabel_4_1);
		
		JLabel lblNewLabel_4_2 = new JLabel("Height :");
		lblNewLabel_4_2.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_4_2.setBounds(523, 425, 63, 19);
		frame.getContentPane().add(lblNewLabel_4_2);
		
		JLabel lblNewLabel_1_2 = new JLabel("Depth (F):");
		lblNewLabel_1_2.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_1_2.setBounds(523, 455, 63, 17);
		frame.getContentPane().add(lblNewLabel_1_2);
		
		JLabel lblNewLabel_1_3 = new JLabel("Depth (D):");
		lblNewLabel_1_3.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_1_3.setBounds(280, 454, 78, 17);
		frame.getContentPane().add(lblNewLabel_1_3);
		
		JLabel lblNewLabel_1_4 = new JLabel("Depth (C):");
		lblNewLabel_1_4.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_1_4.setBounds(280, 182, 69, 17);
		frame.getContentPane().add(lblNewLabel_1_4);
		
		JLabel lblNewLabel_1_5 = new JLabel("Depth  (H):");
		lblNewLabel_1_5.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_1_5.setBounds(523, 182, 78, 17);
		frame.getContentPane().add(lblNewLabel_1_5);
		
		JLabel lblNewLabel_2_2 = new JLabel("Balanced Binary Tree");
		lblNewLabel_2_2.setForeground(Color.BLUE);
		lblNewLabel_2_2.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_2_2.setBounds(280, 213, 145, 19);
		frame.getContentPane().add(lblNewLabel_2_2);
		
		JLabel lblNewLabel_2_3 = new JLabel("Not a Balanced Binary Tree");
		lblNewLabel_2_3.setForeground(Color.RED);
		lblNewLabel_2_3.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_2_3.setBounds(280, 483, 145, 19);
		frame.getContentPane().add(lblNewLabel_2_3);
		
		JLabel lblNewLabel_2_4 = new JLabel("Not a Balanced Binary Tree");
		lblNewLabel_2_4.setForeground(Color.RED);
		lblNewLabel_2_4.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_2_4.setBounds(523, 483, 145, 19);
		frame.getContentPane().add(lblNewLabel_2_4);
		
		JLabel lblNewLabel_2_5 = new JLabel("Not a Balanced Binary Tree");
		lblNewLabel_2_5.setForeground(Color.RED);
		lblNewLabel_2_5.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_2_5.setBounds(523, 210, 145, 19);
		frame.getContentPane().add(lblNewLabel_2_5);
		
		JLabel lblNewLabel_6 = new JLabel("2");
		lblNewLabel_6.setForeground(Color.RED);
		lblNewLabel_6.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_6.setBounds(107, 151, 63, 19);
		frame.getContentPane().add(lblNewLabel_6);
		
		JLabel lblNewLabel_6_1 = new JLabel("2");
		lblNewLabel_6_1.setForeground(Color.RED);
		lblNewLabel_6_1.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_6_1.setBounds(107, 182, 63, 19);
		frame.getContentPane().add(lblNewLabel_6_1);
		
		JLabel lblNewLabel_6_2 = new JLabel("3");
		lblNewLabel_6_2.setForeground(Color.RED);
		lblNewLabel_6_2.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_6_2.setBounds(371, 152, 63, 19);
		frame.getContentPane().add(lblNewLabel_6_2);
		
		JLabel lblNewLabel_6_3 = new JLabel("3");
		lblNewLabel_6_3.setForeground(Color.RED);
		lblNewLabel_6_3.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_6_3.setBounds(371, 182, 63, 19);
		frame.getContentPane().add(lblNewLabel_6_3);
		
		JLabel lblNewLabel_6_4 = new JLabel("3");
		lblNewLabel_6_4.setForeground(Color.RED);
		lblNewLabel_6_4.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_6_4.setBounds(627, 152, 63, 19);
		frame.getContentPane().add(lblNewLabel_6_4);
		
		JLabel lblNewLabel_6_5 = new JLabel("3");
		lblNewLabel_6_5.setForeground(Color.RED);
		lblNewLabel_6_5.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_6_5.setBounds(627, 182, 63, 19);
		frame.getContentPane().add(lblNewLabel_6_5);
		
		JLabel lblNewLabel_6_6 = new JLabel("3");
		lblNewLabel_6_6.setForeground(Color.RED);
		lblNewLabel_6_6.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_6_6.setBounds(118, 422, 63, 19);
		frame.getContentPane().add(lblNewLabel_6_6);
		
		JLabel lblNewLabel_6_7 = new JLabel("3");
		lblNewLabel_6_7.setForeground(Color.RED);
		lblNewLabel_6_7.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_6_7.setBounds(118, 454, 63, 19);
		frame.getContentPane().add(lblNewLabel_6_7);
		
		JLabel lblNewLabel_6_8 = new JLabel("4");
		lblNewLabel_6_8.setForeground(Color.RED);
		lblNewLabel_6_8.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_6_8.setBounds(382, 422, 63, 19);
		frame.getContentPane().add(lblNewLabel_6_8);
		
		JLabel lblNewLabel_6_9 = new JLabel("2");
		lblNewLabel_6_9.setForeground(Color.RED);
		lblNewLabel_6_9.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_6_9.setBounds(382, 455, 63, 19);
		frame.getContentPane().add(lblNewLabel_6_9);
		
		JLabel lblNewLabel_6_10 = new JLabel("3");
		lblNewLabel_6_10.setForeground(Color.RED);
		lblNewLabel_6_10.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_6_10.setBounds(627, 422, 63, 19);
		frame.getContentPane().add(lblNewLabel_6_10);
		
		JLabel lblNewLabel_6_11 = new JLabel("3");
		lblNewLabel_6_11.setForeground(Color.RED);
		lblNewLabel_6_11.setFont(new Font("Gadugi", Font.BOLD, 13));
		lblNewLabel_6_11.setBounds(627, 455, 63, 19);
		frame.getContentPane().add(lblNewLabel_6_11);
		
		JLabel lblNewLabel_2_3_1 = new JLabel("Correct Answers");
		lblNewLabel_2_3_1.setForeground(Color.BLACK);
		lblNewLabel_2_3_1.setFont(new Font("Gadugi", Font.BOLD, 20));
		lblNewLabel_2_3_1.setBounds(267, 516, 165, 33);
		frame.getContentPane().add(lblNewLabel_2_3_1);
		
		JButton btnNextLevel = new JButton("Next Level");
		btnNextLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			JOptionPane.showMessageDialog(null,"Highest level is still in maintainance mode");
			}
		});
		btnNextLevel.setBounds(612, 541, 110, 21);
		frame.getContentPane().add(btnNextLevel);
		
		JButton btnMainMenu2 = new JButton("Main Menu");
		btnMainMenu2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae) {
				frame.dispose();
				LevelWindow lw = new LevelWindow();
			}
		});
		btnMainMenu2.setBounds(22, 542, 109, 21);
		frame.getContentPane().add(btnMainMenu2);
		
	}

}
