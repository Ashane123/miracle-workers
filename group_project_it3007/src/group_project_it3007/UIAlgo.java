package group_project_it3007;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class UIAlgo {
    private JFrame fFrame = new JFrame("AlgoUI");
    private JLabel heading = new JLabel ("");
    private JLabel txt1 = new JLabel("Enter Data :");
    private JTextField dataField = new JTextField (5);
    private JButton submit = new JButton("Submit");
    private JButton ok = new JButton("Show Result");
    private JTextArea output = new JTextArea(7,5);
    private JButton home = new JButton("Home");
    private JTextField dataField2 = new JTextField (5);
    private JButton submit2 = new JButton("Submit");

    //Creating the arrayList
    ArrayList<Integer> myNum = new ArrayList<Integer>();
    public int m2;
    public  JLabel txt2 = new JLabel("Search For :");
    //public static void main(String[]args) {
    	//UIAlgo a = new UIAlgo();
    	
    	
    //}
    
   
    
    public UIAlgo(){

        fFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        fFrame.setSize(700,450);
        fFrame.setVisible(true);
        fFrame.getContentPane();
        fFrame.getContentPane().setLayout (null);
        fFrame.setLocation(280,50);
        heading.setFont(new Font("Calibri", Font.BOLD, 24));
        txt1.setFont(new Font("Calibri", Font.BOLD, 18));
        output.setFont(new Font("Calibri", Font.BOLD, 18));

        heading.setBounds (25, 15, 400, 30);
        dataField.setBounds (150, 60, 150, 30);
        txt1.setBounds (50, 60, 245, 30);
        submit.setBounds (313, 63, 80, 35);
        ok.setBounds (538, 144, 120, 40);
        output.setBounds(27, 216, 600, 180);
        home.setBounds(538, 54, 120, 40);
        dataField2.setBounds (150, 150, 150, 30);

        home.setForeground(new Color(255, 235, 205));
        home.setBackground(new Color(139, 0, 139));
        Border line1 = new LineBorder(Color.BLACK);
        Border margin2 = new EmptyBorder(5, 15, 5, 15);
        Border compound3 = new CompoundBorder(line1, margin2);
        home.setBorder(compound3);

        ok.setForeground(Color.BLACK);
        ok.setBackground(new Color(0, 206, 209));
        Border line = new LineBorder(Color.BLACK);
        Border margin = new EmptyBorder(5, 15, 5, 15);
        Border compound = new CompoundBorder(line, margin);
        ok.setBorder(compound);

        submit.setForeground(Color.BLACK);
        submit.setBackground(new Color(0, 206, 209));
        submit.setBorder(compound);

        heading.setForeground(new Color(0, 128, 128));
        txt1.setForeground(Color.BLUE);
        txt2.setFont(new Font("Calibri", Font.BOLD, 18));
        txt2.setBounds(50, 151, 104, 29);
        
        fFrame.getContentPane().add(txt2);
        submit2.setBounds (313, 147, 80, 35);
        
                submit2.setForeground(Color.BLACK);
                submit2.setBackground(new Color(0, 206, 209));
                submit2.setBorder(compound);
                fFrame.getContentPane().add(submit2);




        fFrame.getContentPane().add(heading);
        fFrame.getContentPane().add(dataField);
        fFrame.getContentPane().add(txt1);
        fFrame.getContentPane().add(submit);
        fFrame.getContentPane().add(ok);
        fFrame.getContentPane().add(output);
        fFrame.getContentPane().add(home);
        fFrame.getContentPane().add(dataField2);
        
        JLabel lblNewLabel = new JLabel("New label");
        lblNewLabel.setIcon(new ImageIcon("D:\\Education\\3rd year\\Algorithem\\project\\PuxL2I.jpg"));
        lblNewLabel.setBounds(0, 190, 686, 223);
        fFrame.getContentPane().add(lblNewLabel);

        //Add action listener
        home.addActionListener(new ActionListener() {
            // ActionPerformed method
            public void actionPerformed(ActionEvent e) {
                fFrame.dispose();
                new Main();
            }
        });
    }

    void fibonacciSearch(){

        heading.setText(" Fibonacci Search Algorithem");

        fFrame.getContentPane().add(txt2);
        fFrame.getContentPane().add(submit2);
        fFrame.getContentPane().add(dataField2);



        //Add action listener
        submit.addActionListener(new ActionListener() {
            // ActionPerformed method
            public void actionPerformed(ActionEvent e) {

                //Get the input as string
                String s1 = dataField.getText();

                //Parse the input string to integer
                int m = Integer.parseInt(s1);

                // Add items to the array
                myNum.add(m);

                // Clear the textfield
                dataField.setText("");

                output.setText("------Data (Sorted Array)------\n");
                for(int i:myNum) {
                    output.append(i + ", ");
                }
            }
        });
        submit2.addActionListener(new ActionListener() {
            // ActionPerformed method
            public void actionPerformed(ActionEvent e) {
                String s2 = dataField2.getText();
                m2 = Integer.parseInt(s2);
            }
        });
        ok.addActionListener(new ActionListener() {
            // ActionPerformed method
            public void actionPerformed(ActionEvent e) {

                output.append("\n\n-----------Output-----------\n");

                Fibonacci fso = new Fibonacci();
                fso.run(myNum,m2);
                if(fso.getOutput()==-1)
                    output.append("Number not found in the Array ");
                else
                    output.append("Found at index: "+fso.getOutput());
            }
        });
    }
}
