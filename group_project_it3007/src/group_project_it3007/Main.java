package group_project_it3007;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JSlider;
import java.awt.Component;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.Box;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JRadioButton;

public class Main {
	Clip clip;

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	  public Main() {
		// frame.add(this);
		//frame.setVisible(true);
		initialize();		
	}
	 

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 697, 525);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setUndecorated(true);
		frame.setLocation(280,50);
		try {
			File sound = new File("Fluffing-a-Duck-[AudioTrimmer.com].wav");
		AudioInputStream ais = AudioSystem.getAudioInputStream(sound);
	    clip = AudioSystem.getClip();
		clip.open(ais);
		clip.start();
		frame.setVisible(true);
			
		}catch(Exception e) {
			System.out.println(e);
		}
		
		JButton btnFibonnaci = new JButton("Fibonnaci");
		btnFibonnaci.setBounds(245, 156, 191, 61);
		btnFibonnaci.setForeground(new Color(0, 0, 102));
		btnFibonnaci.setBackground(new Color(255, 255, 255));
		btnFibonnaci.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnFibonnaci.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clip.stop();
				frame.dispose();
                FiboInstru sobj2 = null;
                try {
                    sobj2 = new FiboInstru();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
                sobj2.fibonacciSearch();
            }

		});
		
		frame.getContentPane().add(btnFibonnaci);
		
		JLabel lblNewLabel = new JLabel("Data Structure and Algorithem");
		lblNewLabel.setBounds(23, 10, 660, 77);
		lblNewLabel.setBackground(Color.RED);
		lblNewLabel.setForeground(Color.CYAN);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 41));
		frame.getContentPane().add(lblNewLabel);
		
		JButton btnGame = new JButton("Game of BBT");
		btnGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clip.stop();
				frame.dispose();
				//MainOfGame game =new MainOfGame();
				//game.MG();
				OpeningWindowNew game = new OpeningWindowNew();
				
			}
		});
		btnGame.setForeground(new Color(0, 0, 102));
		btnGame.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnGame.setBounds(25, 239, 177, 63);
		btnGame.setBackground(new Color(255, 255, 255));
		frame.getContentPane().add(btnGame);
		
		JButton btnBBT = new JButton("Balanced Binary Tree");
		btnBBT.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clip.stop();
				/*try {
					Thread.sleep(10);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}*/
				frame.dispose();
	                BBTWindow sobj1 = new BBTWindow();
	                sobj1.binaryTree();
			}
		});
		btnBBT.setForeground(new Color(0, 0, 102));
		btnBBT.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnBBT.setBackground(new Color(255, 255, 255));
		btnBBT.setBounds(23, 156, 179, 61);
		frame.getContentPane().add(btnBBT);
		
		JButton btnSort2 = new JButton("Marks System");
		btnSort2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				clip.stop();
				Marks obj6 = new Marks();
				obj6.MK();
			}
		});
		btnSort2.setForeground(new Color(0, 0, 102));
		btnSort2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnSort2.setBackground(new Color(255, 255, 255));
		btnSort2.setBounds(465, 324, 191, 61);
		frame.getContentPane().add(btnSort2);
		
		JButton btnSort1 = new JButton("Insertion Sort");
		btnSort1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clip.stop();
				frame.dispose();
				Random_Insertion ob3 = new Random_Insertion();
				ob3.Insertion(); 
			}
		});
		btnSort1.setForeground(new Color(0, 0, 102));
		btnSort1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnSort1.setBackground(new Color(255, 255, 255));
		btnSort1.setBounds(465, 156, 191, 61);
		frame.getContentPane().add(btnSort1);
		
		JButton btnExit = new JButton("Exit");
		btnExit.setBackground(new Color(0, 206, 209));
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
		btnExit.setForeground(new Color(75, 0, 130));
		btnExit.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnExit.setBounds(282, 445, 119, 53);
		frame.getContentPane().add(btnExit);
		
		JButton btnNewButton = new JButton("Graphical visualization");
		btnNewButton.setBackground(new Color(255, 255, 255));
		btnNewButton.setForeground(new Color(0, 0, 102));
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				clip.stop();
				Insertion_visualization obj5 = new Insertion_visualization();
				obj5.Visualization();
			}
		});
		btnNewButton.setBounds(465, 241, 191, 61);
		frame.getContentPane().add(btnNewButton);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 17));
		lblNewLabel_1.setIcon(new ImageIcon("j.jpeg"));
		lblNewLabel_1.setBounds(0, 12, 683, 513);
		frame.getContentPane().add(lblNewLabel_1);
	}

	public void setVisible(boolean b) {
		// TODO Auto-generated method stub
		
	}
}
