package group_project_it3007;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Toolkit;
import javax.swing.ImageIcon;

public class Random_Insertion extends javax.swing.JFrame {

	int num1, num2, num3, num4, num5, num6;
	String q1="", q2="", q3="", q4="", q5="", q6="";
	
	JButton[]num_array;
	int []A;
//	int Swap_mode;
	int i,j,key;
	String n0, n1, n2, n3, n4, n5;
	public void InsertionRandom() {
		initComponents();
	}

	private void initComponents() {
		// TODO Auto-generated method stub
		
	}

	private JFrame frame;
	private JTextField textField_0;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JButton btnReset;
	private JButton btnNewButton_7;
	private JButton btnNewButton_8;
	private JButton btnNewButton_9;
	private JButton btnNewButton_10;
	private JButton btnNewButton_11;
	private JButton btnNewButton_12;
	private JButton btnGenerateNumber;
	private JLabel lblNewLabel;
	private JTextField textField;
	private JTextField textField_6;

	/**
	 * Launch the application.
	 */
	public void Insertion(){
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Random_Insertion window = new Random_Insertion();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Random_Insertion() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(0, 51, 102));
		frame.setBounds(100, 100, 704, 514);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
frame.getContentPane().setLayout(null);
		
		textField_0 = new JTextField();
		textField_0.setBounds(10, 95, 64, 20);
		frame.getContentPane().add(textField_0);
		textField_0.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(141, 95, 64, 20);
		frame.getContentPane().add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(263, 95, 64, 20);
		frame.getContentPane().add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(380, 95, 64, 20);
		frame.getContentPane().add(textField_3);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(497, 95, 64, 20);
		frame.getContentPane().add(textField_4);
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(614, 95, 64, 20);
		frame.getContentPane().add(textField_5);
		
		JButton btnNewButton_0 = new JButton("");
		btnNewButton_0.setBounds(10, 161, 64, 23);
		frame.getContentPane().add(btnNewButton_0);
		
		JButton btnNewButton_1 = new JButton("");
		btnNewButton_1.setBounds(141, 161, 64, 23);
		frame.getContentPane().add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("");
		btnNewButton_2.setBounds(263, 161, 64, 23);
		frame.getContentPane().add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("");
		btnNewButton_3.setBounds(380, 161, 64, 23);
		frame.getContentPane().add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("");
		btnNewButton_4.setBounds(497, 161, 64, 23);
		frame.getContentPane().add(btnNewButton_4);
		
		JButton btnNewButton_5 = new JButton("");
		btnNewButton_5.setBounds(614, 161, 64, 23);
		frame.getContentPane().add(btnNewButton_5);
		
		JButton btnInsertionSort = new JButton("Insertion Sort");
		btnInsertionSort.setBackground(SystemColor.controlHighlight);
		btnInsertionSort.setForeground(new Color(0, 0, 204));
		btnInsertionSort.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnInsertionSort.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				A = new int[6];
				if(textField_0.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Insert each field");
				}
				else if(textField_1.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Insert each field");
				}
				else if(textField_2.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Insert each field");
				}
				else if(textField_3.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Insert each field");
				}
				else if(textField_4.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Insert each field");
				}
				else if(textField_5.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Insert each field");
				}
				else {
					A[0] = Integer.parseInt(textField_0.getText());
					A[1] = Integer.parseInt(textField_1.getText());
					A[2] = Integer.parseInt(textField_2.getText());
					A[3] = Integer.parseInt(textField_3.getText());
					A[4] = Integer.parseInt(textField_4.getText());
					A[5] = Integer.parseInt(textField_5.getText());
					
					btnNewButton_0.setText(textField_0.getText());
					btnNewButton_1.setText(textField_1.getText());
					btnNewButton_2.setText(textField_2.getText());
					btnNewButton_3.setText(textField_3.getText());
					btnNewButton_4.setText(textField_4.getText());
					btnNewButton_5.setText(textField_5.getText());
				}
				
				int x = 0;
				int n = A.length;
				for (int i=1; i<n; ++i)
				{
					int key = A[i];
					int j = i-1;
					while (j>=0 && A[j] > key)
					{
						A[j+1] = A[j];
						j = j-1;
					}
					A[j+1] = key;
					
					x=x+1;
					
				}
				int total = x;

				String value = JOptionPane.showInputDialog(this, "Enter Number of Steps : ");
				
				textField.setText(value);
				
				
				textField_6.setText(Integer.toString(total));
				
				n0 = String.format("%d", A[0]);
				n1 = String.format("%d", A[1]);
				n2 = String.format("%d", A[2]);
				n3 = String.format("%d", A[3]);
				n4 = String.format("%d", A[4]);
				n5 = String.format("%d", A[5]);
				
				btnNewButton_7.setText(n0);
				btnNewButton_8.setText(n1);
				btnNewButton_9.setText(n2);
				btnNewButton_10.setText(n3);
				btnNewButton_11.setText(n4);
				btnNewButton_12.setText(n5);
			}
		});
		btnInsertionSort.setBounds(306, 333, 138, 23);
		frame.getContentPane().add(btnInsertionSort);
		
		btnReset = new JButton("Reset");
		btnReset.setForeground(new Color(0, 0, 204));
		btnReset.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnNewButton_0.setText("");
				btnNewButton_1.setText("");
				btnNewButton_2.setText("");
				btnNewButton_3.setText("");
				btnNewButton_4.setText("");
				btnNewButton_5.setText("");
				
				btnNewButton_7.setText("");
				btnNewButton_8.setText("");
				btnNewButton_9.setText("");
				btnNewButton_10.setText("");
				btnNewButton_11.setText("");
				btnNewButton_12.setText("");
				
				textField_0.setText("");
				textField_1.setText("");
				textField_2.setText("");
				textField_3.setText("");
				textField_4.setText("");
				textField_5.setText("");
				
				textField.setText("");
				textField_6.setText("");
				
				}
		});
		btnReset.setBounds(553, 333, 105, 23);
		frame.getContentPane().add(btnReset);
		
		btnNewButton_7 = new JButton("");
		btnNewButton_7.setBounds(10, 210, 64, 23);
		frame.getContentPane().add(btnNewButton_7);
		
		btnNewButton_8 = new JButton("");
		btnNewButton_8.setBounds(141, 210, 64, 23);
		frame.getContentPane().add(btnNewButton_8);
		
		btnNewButton_9 = new JButton("");
		btnNewButton_9.setBounds(263, 210, 64, 23);
		frame.getContentPane().add(btnNewButton_9);
		
		btnNewButton_10 = new JButton("");
		btnNewButton_10.setBounds(380, 210, 64, 23);
		frame.getContentPane().add(btnNewButton_10);
		
		btnNewButton_11 = new JButton("");
		btnNewButton_11.setBounds(497, 210, 64, 23);
		frame.getContentPane().add(btnNewButton_11);
		
		btnNewButton_12 = new JButton("");
		btnNewButton_12.setBounds(614, 210, 64, 23);
		frame.getContentPane().add(btnNewButton_12);
		
		JLabel lbl_Heading = new JLabel("INSERTION SORT FOR RANDOM NUMBERS AND USER INPUTS");
		lbl_Heading.setForeground(new Color(255, 255, 102));
		lbl_Heading.setFont(new Font("Tahoma", Font.BOLD, 18));
		lbl_Heading.setBounds(62, 0, 596, 52);
		frame.getContentPane().add(lbl_Heading);
		
		btnGenerateNumber = new JButton("Generate Number");
		btnGenerateNumber.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				num1 = 1 + (int) (Math.random() * 48);
				q1 += num1;
				textField_0.setText(q1);
				num2 = 1 + (int) (Math.random() * 48);
				q2 += num2;
				textField_1.setText(q2);
				num3 = 1 + (int) (Math.random() * 48);
				q3 += num3;
				textField_2.setText(q3);
				num4 = 1 + (int) (Math.random() * 48);
				q4 += num4;
				textField_3.setText(q4);
				num5 = 1 + (int) (Math.random() * 48);
				q5 += num5;
				textField_4.setText(q5);
				num6 = 1 + (int) (Math.random() * 48);
				q6 += num6;
				textField_5.setText(q6);
			}
			
		});
		btnGenerateNumber.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				
				textField_0.setText(q1="");
				textField_1.setText(q2="");
				textField_2.setText(q3="");
				textField_3.setText(q4="");
				textField_4.setText(q5="");
				textField_5.setText(q6="");
			}
		});
		btnGenerateNumber.setForeground(new Color(0, 0, 204));
		btnGenerateNumber.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnGenerateNumber.setBackground(new Color(204, 204, 204));
		btnGenerateNumber.setBounds(20, 333, 159, 23);
		frame.getContentPane().add(btnGenerateNumber);
		
		lblNewLabel = new JLabel("Enter Number of Steps :");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel.setForeground(new Color(255, 255, 255));
		lblNewLabel.setBounds(20, 275, 189, 14);
		frame.getContentPane().add(lblNewLabel);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(199, 274, 64, 20);
		frame.getContentPane().add(textField);
		
		textField_6 = new JTextField();
		textField_6.setColumns(10);
		textField_6.setBounds(614, 269, 64, 20);
		frame.getContentPane().add(textField_6);
		
		JButton btnNewButton = new JButton("Home");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				 Main j =new Main();
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton.setBounds(20, 412, 99, 43);
		frame.getContentPane().add(btnNewButton);
		
		JLabel lblNewLabel_1 = new JLabel("Number of Steps  :");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_1.setForeground(new Color(255, 255, 255));
		lblNewLabel_1.setBounds(451, 275, 167, 14);
		frame.getContentPane().add(lblNewLabel_1);
		
		JButton btnNewButton_6 = new JButton("Instructions");
		btnNewButton_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				Sort_Instrutions obj4 = new Sort_Instrutions();
				obj4.Sortins();
			}
		});
		btnNewButton_6.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton_6.setBounds(524, 412, 134, 43);
		frame.getContentPane().add(btnNewButton_6);
		
		JLabel lblNewLabel_2 = new JLabel("New label");
		lblNewLabel_2.setIcon(new ImageIcon("D:\\Education\\IT3007_Project\\group_project_it3007\\algorithms.jpeg"));
		lblNewLabel_2.setBounds(20, 27, 644, 450);
		frame.getContentPane().add(lblNewLabel_2);
		
	}
}
