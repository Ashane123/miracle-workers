package group_project_it3007;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

public class FiboInstru {

    private JFrame sFrame = new JFrame("Fibonnaci Instruction");
    private JButton home = new JButton("Home");
    private JButton fui = new JButton("Fibonnaci");

    private JLabel text1 = new JLabel("Fibonnaci Search Algorithm ");
    private JLabel text2 = new JLabel("1)Input Data one by one using the Submit  button.");
    private JLabel text3 = new JLabel("2)The inputs must be sorted.");
    private JLabel text4 = new JLabel("3)Enter the number you want to search for.");
    private JLabel text5 = new JLabel("4)Press Show Results to get the output.");
   


    public FiboInstru() throws IOException {

        sFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        sFrame.setSize(620,370);
        sFrame.setVisible(true);
        sFrame.getContentPane();
        sFrame.getContentPane().setLayout (null);
        //sFrame.setUndecorated(true);



        text1.setFont(new Font("Calibri", Font.BOLD, 32));
        text2.setForeground(new Color(255, 255, 255));
        text2.setFont(new Font("Calibri", Font.PLAIN, 27));
        text3.setForeground(new Color(255, 255, 255));
        text3.setFont(new Font("Calibri", Font.PLAIN, 27));
        text4.setForeground(new Color(255, 255, 255));
        text4.setFont(new Font("Calibri", Font.PLAIN, 27));
        text5.setFont(new Font("Calibri", Font.PLAIN, 30));
        home.setFont(new Font("Calibri", Font.PLAIN, 25));
        fui.setFont(new Font("Calibri", Font.PLAIN, 25));


        fui.setForeground(Color.BLACK);
        fui.setBackground(new Color(0, 206, 209));
        Border line = new LineBorder(Color.BLACK);
        Border margin = new EmptyBorder(5, 15, 5, 15);
        Border compound = new CompoundBorder(line, margin);
        fui.setBorder(compound);

        home.setForeground(Color.BLACK);
        home.setBackground(new Color(0, 206, 209));
        Border line1 = new LineBorder(Color.BLACK);
        Border margin2 = new EmptyBorder(5, 15, 5, 15);
        Border compound3 = new CompoundBorder(line1, margin2);
        home.setBorder(compound3);


        text1.setBounds (30, 30, 645, 30);
        text2.setBounds (30, 70, 566, 30);
        text3.setBounds (30, 120, 566, 30);
        text4.setBounds (30, 178, 566, 30);
        text5.setBounds (30, 190, 645, 30);
        home.setBounds(30, 267, 200, 40);
        fui.setBounds(375, 267, 200, 40);
        text1.setForeground(new Color(204, 255, 255));

        sFrame.getContentPane().add(text1);
        sFrame.getContentPane().add(text2);
        sFrame.getContentPane().add(text3);
        sFrame.getContentPane().add(text4);
        sFrame.getContentPane().add(home);
        sFrame.getContentPane().add(fui);
        
        JLabel lblNewLabel = new JLabel("");
        lblNewLabel.setIcon(new ImageIcon("D:\\Education\\3rd year\\Algorithem\\project\\o.jpg"));
        lblNewLabel.setBounds(0, -13, 630, 356);
        sFrame.getContentPane().add(lblNewLabel);


        //Add action listener
        home.addActionListener(new ActionListener() {
            // ActionPerformed method
            public void actionPerformed(ActionEvent e) {
                sFrame.dispose();
                new Main();
            }
        });
    }



    void fibonacciSearch(){

        //Add action listener
        home.addActionListener(new ActionListener() {
            // ActionPerformed method
            public void actionPerformed(ActionEvent e) {
                sFrame.dispose();
                new Main();
            }
        });

        fui.addActionListener(
                new ActionListener() {
                    // ActionPerformed method
                    public void actionPerformed(ActionEvent e) {
                        sFrame.dispose();
                        UIAlgo uiAlgo = new UIAlgo();
                        uiAlgo.fibonacciSearch();
                    }
                }
        );

    }


    public static void main (String[] args) throws IOException {
       new FiboInstru();

    }
}