package group_project_it3007;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class BBTWindow {

    private JFrame sFrame = new JFrame("Second");
    private JLabel heading = new JLabel ("");
    private JLabel txt1 = new JLabel("Enter Data :");
    private JTextField dataField = new JTextField (5);
    private JButton submit = new JButton("Submit");
    private JButton ok = new JButton("Show Result");
    private JTextArea output = new JTextArea(7,5);
    private JButton home = new JButton("Home");
    private JLabel txt2 = new JLabel("Search for :");
    private JTextField dataField2 = new JTextField (5);
    private JButton submit2 = new JButton("Submit");

    //Creating the arrayList
    ArrayList<Integer> myNum = new ArrayList<Integer>();
    public int m2;

    public BBTWindow(){

        sFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        sFrame.setSize(700,500);
        sFrame.setVisible(true);
        sFrame.getContentPane();
        sFrame.getContentPane().setLayout (null);
        heading.setForeground(new Color(255, 255, 255));

        heading.setFont(new Font("Calibri", Font.BOLD, 24));
        txt1.setForeground(new Color(245, 255, 250));
        txt1.setFont(new Font("Calibri", Font.BOLD, 25));
        txt2.setFont(new Font("Calibri", Font.PLAIN, 18));
        output.setBackground(new Color(224, 255, 255));
        output.setFont(new Font("Calibri", Font.BOLD, 18));

        heading.setBounds (25, 15, 625, 30);
        dataField.setBackground(new Color(224, 255, 255));
        dataField.setBounds (452, 57, 198, 33);
        txt1.setBounds (336, 60, 314, 30);
        submit.setBackground(new Color(0, 206, 209));
        submit.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        	}
        });
        submit.setBounds (484, 102, 80, 35);
        ok.setBackground(new Color(0, 206, 209));
        ok.setBounds (517, 206, 119, 40);
        output.setBounds(37, 256, 599, 180);
        home.setBackground(new Color(0, 206, 209));
        home.setBounds(570, 102, 80, 35);
        txt2.setBounds(50, 150, 245, 30);
        submit2.setBounds (150, 200, 80, 35);
        dataField2.setBounds (150, 150, 150, 35);

        sFrame.getContentPane().add(heading);
        sFrame.getContentPane().add(dataField);
        sFrame.getContentPane().add(txt1);
        sFrame.getContentPane().add(submit);
        sFrame.getContentPane().add(ok);
        sFrame.getContentPane().add(output);
        sFrame.getContentPane().add(home);
        
        JButton btnInstruction = new JButton("How To Play");
        btnInstruction.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		JOptionPane.showMessageDialog(null,"1.Input data using Submit Button" + "\n 2.The inputs must be sorted" +"\n 3. Press Show result option to get the output.");
        	}
        });
        btnInstruction.setFont(new Font("Tahoma", Font.PLAIN, 20));
        btnInstruction.setBackground(new Color(0, 206, 209));
        btnInstruction.setBounds(27, 87, 299, 96);
        sFrame.getContentPane().add(btnInstruction);
        
        JLabel lblNewLabel = new JLabel("New label");
        lblNewLabel.setIcon(new ImageIcon("D:\\Education\\3rd year\\Algorithem\\project\\s.jpg"));
        lblNewLabel.setBounds(0, 10, 676, 443);
        sFrame.getContentPane().add(lblNewLabel);

        //Add action listener
        home.addActionListener(new ActionListener() {
            // ActionPerformed method
            public void actionPerformed(ActionEvent e) {
                sFrame.dispose();
               Main m =new Main();
               m.setVisible(true);
                //new main();
            }
        });
    }

    void binaryTree(){

        heading.setText("Preorder Traversal For Balance Binary Tree");
        /*output.setText(" 1.Input data using the submit Button" +
                "\n -> The inputs must be sorted"+
                "\n -> Press \"Show Results\" to get the output.");*/

        //Add action listener
        submit.addActionListener(new ActionListener() {
            // ActionPerformed method
            public void actionPerformed(ActionEvent e) {

                //Get the input as string
                String s1 = dataField.getText();

                //Parse the input string to integer
                int m = Integer.parseInt(s1);

                // Add items to the array
                myNum.add(m);

                // Clear the textfield
                dataField.setText("");

                output.setText("------Initial Sorted Data------\n");
                for(int i:myNum) {
                    output.append(i + ", ");
                }
            }
        });

        ok.addActionListener(new ActionListener() {
            // ActionPerformed method
            public void actionPerformed(ActionEvent e) {

                output.append("\n\n--Preorder traversal of constructed Binary Search Tree--\n");

                BBT obj = new BBT();
                obj.startMake(myNum);

                for (int i : obj.outArray) {
                    output.append(i + " ");
                }
            }
        });
    }
    }
