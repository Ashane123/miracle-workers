package group_project_it3007;
import java.util.ArrayList;

// A binary tree node
class Node {

    int data;
    Node left, right;

    Node(int d) {
        data = d;
        left = right = null;
    }
}

class BBT {
    public static ArrayList<Integer> outArray = new ArrayList<Integer>();
    static Node root;

    /* A function that constructs Balanced Binary Search Tree
     from a sorted array */
    Node sortedArrayToBST(ArrayList<Integer> arr, int start, int end) {

        /* Base Case */
        if (start > end) {
            return null;
        }

        /* Get the middle element and make it root */
        int mid = (start + end) / 2;
        Node node = new Node(arr.get(mid));

        /* Recursively construct the left subtree and make it
         left child of root */
        node.left = sortedArrayToBST(arr, start, mid - 1);

        /* Recursively construct the right subtree and make it
         right child of root */
        node.right = sortedArrayToBST(arr, mid + 1, end);

        return node;
    }

    /* A utility function to print preorder traversal of BST */
    ArrayList<Integer> preOrder(Node node) {

        if (node == null) {
            return null;
        }
        System.out.print(node.data + " ");
        int k = node.data;
        outArray.add(k);

        preOrder(node.left);
        preOrder(node.right);
        return outArray;
    }

    void startMake(ArrayList<Integer> array) {
        BBT tree = new BBT();
        ArrayList<Integer> arr = array;
        int n = arr.size();
        root = tree.sortedArrayToBST(arr, 0, n - 1);
        System.out.println("Preorder traversal of constructed BST");
        tree.preOrder(root);
    }

}
