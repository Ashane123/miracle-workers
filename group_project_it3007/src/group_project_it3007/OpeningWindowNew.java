package group_project_it3007;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Timer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Color;

public class OpeningWindowNew {
	Clip clip;
	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					OpeningWindowNew window = new OpeningWindowNew();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public OpeningWindowNew() {
		initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(0, 0, 757, 541);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setUndecorated(true);
		frame.setLocation(280,50);
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon("start2.jpg"));
		lblNewLabel.setBounds(0, 0, 757, 541);
		frame.getContentPane().add(lblNewLabel);
		
		
		
		try {
			File sound = new File("mixkit-children-happy-countdown-923.wav");
		AudioInputStream ais = AudioSystem.getAudioInputStream(sound);
		clip = AudioSystem.getClip();
		clip.open(ais);
		clip.start();
		frame.setVisible(true);
		
		}catch(Exception e) {
			System.out.println(e);
		}
		
		JButton btnNext = new JButton("Next");
		btnNext.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae) {
				frame.dispose();
				clip.stop();
				LevelWindow lw = new LevelWindow();
			}
		});
		btnNext.setBackground(new Color(0, 191, 255));
		btnNext.setFont(new Font("Sylfaen", Font.BOLD, 22));
		btnNext.setBounds(565, 473, 120, 45);
		frame.getContentPane().add(btnNext);
		
	}
}
