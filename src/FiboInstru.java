import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

public class FiboInstru {

    private JFrame sFrame = new JFrame("Fibonnaci Instruction");
    private JButton home = new JButton("Home");
    private JButton fui = new JButton("Fibonnaci");

    private JLabel text1 = new JLabel("Fibonnaci Search Algorithm ");
    private JLabel text2 = new JLabel("1)Input Data one by one using the Submit  button.");
    private JLabel text3 = new JLabel("2)The inputs must be sorted.");
    private JLabel text4 = new JLabel("3)Enter the number you want to search for.");
    private JLabel text5 = new JLabel("4)Press Show Results to get the output.");


    public FiboInstru() throws IOException {

        sFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        sFrame.setSize(700,350);
        sFrame.setVisible(true);
        sFrame.getContentPane();
        sFrame.setLayout (null);




        text1.setFont(new Font("Calibri", Font.PLAIN, 30));
        text2.setFont(new Font("Calibri", Font.PLAIN, 30));
        text3.setFont(new Font("Calibri", Font.PLAIN, 30));
        text4.setFont(new Font("Calibri", Font.PLAIN, 30));
        text5.setFont(new Font("Calibri", Font.PLAIN, 30));
        home.setFont(new Font("Calibri", Font.PLAIN, 25));
        fui.setFont(new Font("Calibri", Font.PLAIN, 25));


        fui.setForeground(Color.BLACK);
        fui.setBackground(Color.GREEN);
        Border line = new LineBorder(Color.BLACK);
        Border margin = new EmptyBorder(5, 15, 5, 15);
        Border compound = new CompoundBorder(line, margin);
        fui.setBorder(compound);

        home.setForeground(Color.BLACK);
        home.setBackground(Color.YELLOW);
        Border line1 = new LineBorder(Color.BLACK);
        Border margin2 = new EmptyBorder(5, 15, 5, 15);
        Border compound3 = new CompoundBorder(line1, margin2);
        home.setBorder(compound3);


        text1.setBounds (30, 30, 645, 30);
        text2.setBounds (30, 70, 645, 30);
        text3.setBounds (30, 110, 645, 30);
        text4.setBounds (30, 150, 645, 30);
        text5.setBounds (30, 190, 645, 30);
        home.setBounds(150, 240, 200, 40);
        fui.setBounds(400, 240, 200, 40);
        text1.setForeground(Color.BLUE);

        sFrame.add(text1);
        sFrame.add(text2);
        sFrame.add(text3);
        sFrame.add(text4);
        sFrame.add(home);
        sFrame.add(fui);


        //Add action listener
        home.addActionListener(new ActionListener() {
            // ActionPerformed method
            public void actionPerformed(ActionEvent e) {
                sFrame.dispose();
                new MainFrame();
            }
        });
    }



    void fibonacciSearch(){

        //Add action listener
        home.addActionListener(new ActionListener() {
            // ActionPerformed method
            public void actionPerformed(ActionEvent e) {
                sFrame.dispose();
                new MainFrame();
            }
        });

        fui.addActionListener(
                new ActionListener() {
                    // ActionPerformed method
                    public void actionPerformed(ActionEvent e) {
                        sFrame.dispose();
                        UIAlgo uiAlgo = new UIAlgo();
                        uiAlgo.fibonacciSearch();
                    }
                }
        );

    }


    public static void main (String[] args) throws IOException {
       new FiboInstru();

    }

}

