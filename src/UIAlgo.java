import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class UIAlgo {
    private JFrame fFrame = new JFrame("AlgoUI");
    private JLabel heading = new JLabel ("");
    private JLabel txt1 = new JLabel("Enter Data :");
    private JTextField dataField = new JTextField (5);
    private JButton submit = new JButton("Submit");
    private JButton ok = new JButton("Show Result");
    private JTextArea output = new JTextArea(7,5);
    private JButton home = new JButton("Home");
    private JLabel txt2 = new JLabel("Search for :");
    private JTextField dataField2 = new JTextField (5);
    private JButton submit2 = new JButton("Submit");

    //Creating the arrayList
    ArrayList<Integer> myNum = new ArrayList<Integer>();
    public int m2;

    public UIAlgo(){

        fFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        fFrame.setSize(700,500);
        fFrame.setVisible(true);
        fFrame.getContentPane();
        fFrame.setLayout (null);

        heading.setFont(new Font("Calibri", Font.BOLD, 24));
        txt1.setFont(new Font("Calibri", Font.PLAIN, 18));
        txt2.setFont(new Font("Calibri", Font.PLAIN, 18));
        output.setFont(new Font("Calibri", Font.BOLD, 18));

        heading.setBounds (25, 15, 400, 30);
        dataField.setBounds (150, 60, 150, 30);
        txt1.setBounds (50, 60, 245, 30);
        submit.setBounds (150, 100, 80, 35);
        ok.setBounds (350, 180, 120, 40);
        output.setBounds(25, 250, 600, 180);
        home.setBounds(350, 60, 120, 40);
        txt2.setBounds(50, 150, 245, 30);
        submit2.setBounds (150, 200, 80, 35);
        dataField2.setBounds (150, 150, 150, 30);

        home.setForeground(Color.BLACK);
        home.setBackground(Color.YELLOW);
        Border line1 = new LineBorder(Color.BLACK);
        Border margin2 = new EmptyBorder(5, 15, 5, 15);
        Border compound3 = new CompoundBorder(line1, margin2);
        home.setBorder(compound3);

        ok.setForeground(Color.BLACK);
        ok.setBackground(Color.GREEN);
        Border line = new LineBorder(Color.BLACK);
        Border margin = new EmptyBorder(5, 15, 5, 15);
        Border compound = new CompoundBorder(line, margin);
        ok.setBorder(compound);

        submit.setForeground(Color.BLACK);
        submit.setBackground(Color.RED);
        submit.setBorder(compound);

        submit2.setForeground(Color.BLACK);
        submit2.setBackground(Color.RED);
        submit2.setBorder(compound);

        heading.setForeground(Color.BLACK);
        txt1.setForeground(Color.BLUE);
        txt2.setForeground(Color.BLUE);




        fFrame.add(heading);
        fFrame.add(dataField);
        fFrame.add(txt1);
        fFrame.add(submit);
        fFrame.add(ok);
        fFrame.add(output);
        fFrame.add(home);
        fFrame.add(txt2);
        fFrame.add(submit2);
        fFrame.add(dataField2);

        //Add action listener
        home.addActionListener(new ActionListener() {
            // ActionPerformed method
            public void actionPerformed(ActionEvent e) {
                fFrame.dispose();
                new MainFrame();
            }
        });
    }

    void fibonacciSearch(){

        heading.setText("Demonstration of Fibonacci Search");

        fFrame.add(txt2);
        fFrame.add(submit2);
        fFrame.add(dataField2);



        //Add action listener
        submit.addActionListener(new ActionListener() {
            // ActionPerformed method
            public void actionPerformed(ActionEvent e) {

                //Get the input as string
                String s1 = dataField.getText();

                //Parse the input string to integer
                int m = Integer.parseInt(s1);

                // Add items to the array
                myNum.add(m);

                // Clear the textfield
                dataField.setText("");

                output.setText("------Data (Sorted Array)------\n");
                for(int i:myNum) {
                    output.append(i + ", ");
                }
            }
        });
        submit2.addActionListener(new ActionListener() {
            // ActionPerformed method
            public void actionPerformed(ActionEvent e) {
                String s2 = dataField2.getText();
                m2 = Integer.parseInt(s2);
            }
        });
        ok.addActionListener(new ActionListener() {
            // ActionPerformed method
            public void actionPerformed(ActionEvent e) {

                output.append("\n\n-----------Output-----------\n");

                Fibonacci fso = new Fibonacci();
                fso.run(myNum,m2);
                if(fso.getOutput()==-1)
                    output.append("Number not found in the Array ");
                else
                    output.append("Found at index: "+fso.getOutput());
            }
        });
    }
}
