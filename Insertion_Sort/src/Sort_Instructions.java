import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Font;
import javax.swing.JButton;

public class Sort_Instructions {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Sort_Instructions window = new Sort_Instructions();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Sort_Instructions() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(0, 51, 102));
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("INSTRUCTIONS");
		lblNewLabel.setBounds(270, 11, 158, 32);
		lblNewLabel.setForeground(new Color(255, 255, 102));
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("FOR USER INPUTS :");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel_1.setForeground(new Color(255, 255, 255));
		lblNewLabel_1.setBounds(10, 50, 143, 14);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_1_1 = new JLabel("FOR GENERATE NUMBERS :");
		lblNewLabel_1_1.setForeground(Color.WHITE);
		lblNewLabel_1_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel_1_1.setBounds(10, 225, 213, 14);
		frame.getContentPane().add(lblNewLabel_1_1);
		
		JLabel lblNewLabel_3 = new JLabel("Enter numbers to all 6 text boxes in the 1st row.");
		lblNewLabel_3.setForeground(new Color(255, 222, 173));
		lblNewLabel_3.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel_3.setBounds(36, 75, 500, 14);
		frame.getContentPane().add(lblNewLabel_3);
		
		JLabel lblNewLabel_3_1 = new JLabel("Press the \u201CInsertion Sort\u201D button. Then entered numbers will appear in the 2nd row without any change.");
		lblNewLabel_3_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel_3_1.setForeground(new Color(255, 222, 173));
		lblNewLabel_3_1.setBounds(36, 100, 642, 14);
		frame.getContentPane().add(lblNewLabel_3_1);
		
		JLabel lblNewLabel_3_1_1 = new JLabel("When the Ok button of the dialog box is clicked, Entered numbers will be sorted and  displayed in the 3rd row.");
		lblNewLabel_3_1_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel_3_1_1.setForeground(new Color(255, 222, 173));
		lblNewLabel_3_1_1.setBounds(36, 150, 642, 14);
		frame.getContentPane().add(lblNewLabel_3_1_1);
		
		JLabel lblNewLabel_3_1_1_1 = new JLabel("Also, the number of steps used for insertion sort will be displayed in the relevant text box.");
		lblNewLabel_3_1_1_1.setForeground(new Color(255, 222, 173));
		lblNewLabel_3_1_1_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel_3_1_1_1.setBounds(36, 175, 642, 14);
		frame.getContentPane().add(lblNewLabel_3_1_1_1);
		
		JLabel lblNewLabel_3_1_1_1_1 = new JLabel("All text boxes will be cleared by clicking  the \u201CReset\u201D button.");
		lblNewLabel_3_1_1_1_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel_3_1_1_1_1.setForeground(new Color(255, 222, 173));
		lblNewLabel_3_1_1_1_1.setBounds(36, 200, 642, 14);
		frame.getContentPane().add(lblNewLabel_3_1_1_1_1);
		
		JLabel lblNewLabel_3_2 = new JLabel("When the \u201CGenerate Numbers\u201D button is clicked random numbers will appear in all 6 text boxes in the 1st row.");
		lblNewLabel_3_2.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_3_2.setForeground(new Color(255, 222, 173));
		lblNewLabel_3_2.setBounds(36, 250, 628, 14);
		frame.getContentPane().add(lblNewLabel_3_2);
		
		JLabel lblNewLabel_2 = new JLabel("A dialog box will be displayed asking for the number of steps for Insertion sort.");
		lblNewLabel_2.setForeground(new Color(255, 222, 173));
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel_2.setBounds(36, 125, 551, 14);
		frame.getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3_1_2 = new JLabel("Press the \u201CInsertion Sort\u201D button. Then entered numbers will appear in the 2nd row without any change.");
		lblNewLabel_3_1_2.setForeground(new Color(255, 222, 173));
		lblNewLabel_3_1_2.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel_3_1_2.setBounds(36, 275, 642, 14);
		frame.getContentPane().add(lblNewLabel_3_1_2);
		
		JLabel lblNewLabel_2_1 = new JLabel("A dialog box will be displayed asking for the number of steps for Insertion sort.");
		lblNewLabel_2_1.setForeground(new Color(255, 222, 173));
		lblNewLabel_2_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel_2_1.setBounds(36, 300, 551, 14);
		frame.getContentPane().add(lblNewLabel_2_1);
		
		JLabel lblNewLabel_3_1_1_2 = new JLabel("When the Ok button of the dialog box is clicked, Entered numbers will be sorted and  displayed in the 3rd row.");
		lblNewLabel_3_1_1_2.setForeground(new Color(255, 222, 173));
		lblNewLabel_3_1_1_2.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel_3_1_1_2.setBounds(36, 325, 642, 14);
		frame.getContentPane().add(lblNewLabel_3_1_1_2);
		
		JLabel lblNewLabel_3_1_1_1_2 = new JLabel("Also, the number of steps used for insertion sort will be displayed in the relevant text box.");
		lblNewLabel_3_1_1_1_2.setForeground(new Color(255, 222, 173));
		lblNewLabel_3_1_1_1_2.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel_3_1_1_1_2.setBounds(36, 350, 642, 14);
		frame.getContentPane().add(lblNewLabel_3_1_1_1_2);
		
		JLabel lblNewLabel_3_1_1_1_1_1 = new JLabel("All text boxes will be cleared by clicking  the \u201CReset\u201D button.");
		lblNewLabel_3_1_1_1_1_1.setForeground(new Color(255, 222, 173));
		lblNewLabel_3_1_1_1_1_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel_3_1_1_1_1_1.setBounds(36, 375, 642, 14);
		frame.getContentPane().add(lblNewLabel_3_1_1_1_1_1);
		
		JButton btnNewButton = new JButton("BACK");
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnNewButton.setBounds(310, 441, 89, 23);
		frame.getContentPane().add(btnNewButton);
		frame.setBounds(100, 100, 704, 514);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
